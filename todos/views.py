from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {
        'todo_list': todo_list  
    }
    
    return render(request, "lists/list.html", context)

def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    context = {
        'list_detail': list_detail
    }
    return render(request, "lists/detail.html", context)

def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list=form.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm()
    context = {
        'form': form
    }
    return render(request, "lists/create.html", context)

def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)
    
    context = {
        "form": form
    }
    return render(request, "lists/edit.html", context)

def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "lists/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect('todo_list_detail', id = todo_item.list.id)
    else:
        form = TodoItemForm()
    todo_list = TodoList.objects.all()
    context = {
        "form": form,
        "todo_list": todo_list,
    }
    return render(request, 'lists/create_item.html', context)

def todo_item_update(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.id)
    else:
        form = TodoItemForm(instance=todo_item)
        
    context = {
        'form': form
    }
    return render(request, "lists/edit_item.html", context)